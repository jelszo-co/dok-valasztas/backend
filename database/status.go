/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package database

import (
	"database/sql"
	"fmt"
	"gitlab.com/jelszo-co/dok-valasztas/backend/server/types"
)

//GetCurrentStatus returns the current status.. If it doesn't exist, it is created.
func GetCurrentStatus() (types.StatusType, error) {
	var status types.StatusType
	err := db.Get(&status, "SELECT value FROM settings where key = 'current'")
	if err == sql.ErrNoRows {
		logger.Warnf("No current status found, seting it to 'CLOSED'")
		_, err = db.Exec("INSERT INTO settings(key,value) VALUES ('current','CLOSED')")
		if err == nil {
			return types.StatusClosed, nil
		}
		logger.Error("Could not set status to default")
	}
	if err != nil {
		logger.Error("Could not get current status")
	}
	return status, err
}

//SetCurrentStatus sets the current status
func SetCurrentStatus(stat types.StatusType) error {
	_, err := db.Exec("INSERT INTO settings(key, value) values ('current',$1) ON CONFLICT (key) DO UPDATE SET value = EXCLUDED.value", stat)
	if err != nil {
		logger.Error("Could not set current status")
	}
	return err
}

func GetTimings() (timings []*types.StatusTiming, err error) {
	timings = make([]*types.StatusTiming, 0)
	err = db.Select(&timings, "SELECT * FROM timing ORDER BY ts_start")
	fmt.Println("")
	return
}

func ClearTimings() error {
	_, err := db.Exec("DELETE FROM timing")
	if err != nil {
		logger.Error("Could not clear timings")
	}
	return err
}

func AddTiming(t *types.StatusTiming) error {
	_, err := db.NamedExec("INSERT INTO timing(name, ts_start, ts_until) values (:name,:ts_start,:ts_until) ON CONFLICT (name) DO UPDATE SET ts_start = EXCLUDED.ts_start,ts_until = EXCLUDED.ts_until", t)
	if err != nil {
		logger.Error("Could not upsert timing")
	}
	return err
}

func RemoveTiming(t types.StatusType) error {
	_, err := db.Exec("DELETE FROM timing WHERE name=$1", t)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Warn("Tried to delete non-existent timing")
			return nil
		}
		logger.Error("Could not delete timing")
	}
	return err
}
