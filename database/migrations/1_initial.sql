/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

-- +migrate Up
CREATE TABLE IF NOT EXISTS candidate
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR NOT NULL UNIQUE,
    description TEXT,
    picture     VARCHAR,
    essay       TEXT    NOT NULL
);

CREATE TABLE IF NOT EXISTS voter
(
    om    VARCHAR(11) PRIMARY KEY,
    dob   DATE NOT NULL,
    voted BOOLEAN DEFAULT false
);

CREATE TABLE IF NOT EXISTS vote
(
    voterhash VARCHAR(40) PRIMARY KEY,
    ts        TIMESTAMP NOT NULL DEFAULT now(),
    candidate INT       NOT NULL,
    CONSTRAINT fk_candidate
        FOREIGN KEY (candidate)
            REFERENCES candidate (id)
            ON DELETE cascade
            ON UPDATE cascade
);

CREATE TABLE IF NOT EXISTS failed
(
    ip VARCHAR   NOT NULL,
    ts TIMESTAMP NOT NULL DEFAULT now()
);

-- +migrate StatementBegin
CREATE FUNCTION is_banned(curr_ip VARCHAR) RETURNS BOOLEAN AS
$$
DECLARE
    c INT;
BEGIN
    SELECT COUNT(*) from failed where ts > now() - interval '10 minute' and ip = curr_ip into c;
    RETURN c >= 5;
END;
$$ LANGUAGE plpgsql;
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE failed;
DROP TABLE vote;
DROP TABLE voter;
DROP TABLE candidate;
DROP FUNCTION is_banned;