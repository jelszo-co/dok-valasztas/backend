/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package database

import "gitlab.com/jelszo-co/dok-valasztas/backend/server/types"

//GetCandidates returns all the candidates.
func GetCandidates() (candidates []*types.Candidate, err error) {
	candidates = make([]*types.Candidate, 0)
	err = db.Select(&candidates, "SELECT id,name,picture,description from candidate ORDER BY name")
	if err != nil {
		logger.Error("Could not get candidates")
	}
	return
}

func GetCandidateEssay(id int) (*types.CandidateEssay, error) {
	ce := new(types.CandidateEssay)
	err := db.Get(ce, "SELECT essay FROM candidate WHERE id=$1", id)
	if err != nil {
		logger.Error("Could not get essay")
	}
	return ce, err
}

//GetWinner returns the winner(s) based on the number of vote objects
func GetWinner() ([]*types.CandidateID, error) {
	candidates := make([]*types.CandidateID, 0)
	err := db.Select(&candidates, `
	SELECT DISTINCT vote.candidate
	FROM vote
	INNER JOIN (SELECT candidate, count(*) as c FROM vote GROUP BY candidate) cnt on cnt.candidate = vote.candidate
	WHERE cnt.c = (SELECT count(*) as c FROM vote GROUP BY candidate ORDER BY c DESC LIMIT 1)
	ORDER BY candidate`,
	)
	if err != nil {
		logger.Error("Could not get winning candidate")
	}
	return candidates, err
}

//GetResults returns the vote counts for each candidate
func GetResults() ([]types.CandidateResult, error) {
	results := make([]types.CandidateResult, 0)
	err := db.Select(&results, "SELECT candidate.id, count(vote.candidate) as result FROM vote RIGHT OUTER JOIN candidate on candidate.id = vote.candidate GROUP BY candidate.id")
	if err != nil {
		logger.Error("Could not get results")
	}
	return results, err
}
