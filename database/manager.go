/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package database

import (
	"errors"
	"github.com/gobuffalo/packr/v2"
	"github.com/jmoiron/sqlx"
	migrate "github.com/rubenv/sql-migrate"
	"go.uber.org/zap"
	"os"
)

var (
	db     *sqlx.DB
	logger *zap.SugaredLogger
)

//Connect to database. Must not be called more than once.
func Connect(sl *zap.SugaredLogger) (err error) {
	logger = sl
	connstr := os.Getenv("DATABASE")
	if connstr == "" {
		logger.Error("No database connection string")
		return errors.New("no database connection string")
	}
	db, err = sqlx.Open("postgres", connstr)
	if err != nil {
		logger.Error("Database connection error", err)
		return err
	}
	migrations := &migrate.PackrMigrationSource{
		Box: packr.New("migrations", "migrations"),
	}
	n, err := migrate.Exec(db.DB, "postgres", migrations, migrate.Up)
	if err != nil {
		logger.Error("Migration failed", err)
		return err
	}
	logger.Infof("%d migrations were applied", n)
	return nil
}
