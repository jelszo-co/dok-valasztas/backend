/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package database

import (
	"crypto/sha1"
	"database/sql"
	"errors"
	"github.com/gobuffalo/packr/v2/file/resolver/encoding/hex"
	"github.com/lib/pq"
	"gitlab.com/jelszo-co/dok-valasztas/backend/server/types"
	"time"
)

//ErrForeignKeyViolation is an error for foreign key or unique constraint breaks
var ErrForeignKeyViolation = errors.New("violated foreign key")

//Fail2BanCheck is the db backend of handlers.Fail2BanMiddleware
func Fail2BanCheck(ip string) (int, error) {
	var isBanned bool
	err := db.Get(&isBanned, "SELECT is_banned($1)", ip)
	if err != nil {
		logger.Error("Could not get banned status")
		return 0, err
	}
	if isBanned {
		var ts time.Time
		err = db.Get(&ts, "SELECT ts FROM failed WHERE ts > now() - interval '10 minute' and ip = $1 ORDER BY ts LIMIT 1", ip)
		if err != nil {
			logger.Error("Could not get timestamp")
			return 0, err
		}
		return int(ts.Add(10 * time.Minute).Sub(time.Now()).Seconds()), nil
	}
	return -1, err
}

//Fail2BanFail creates a fail record for the ip
func Fail2BanFail(ip string) error {
	_, err := db.Exec("INSERT INTO failed(ip) VALUES ($1)", ip)
	if err != nil {
		logger.Error("Could not add fail")
	}
	return err
}

//CheckVoter returns whether the user can vote or not
func CheckVoter(v *types.Voter) (types.VoterStatus, error) {
	voted := new(bool)
	err := db.Get(voted, "SELECT voted FROM voter WHERE om=$1", v.Om)
	if err != nil {
		if err == sql.ErrNoRows {
			return types.WrongData, nil
		}
		logger.Error("Error getting voter data")
		return -1, err
	}
	if *voted {
		return types.AlreadyVoted, err
	}
	return types.CanVote, err
}

//Vote is saving the vote in the db and updating the voter record
func Vote(v *types.Vote) error {
	d := sha1.Sum([]byte(v.Om))
	hash := hex.EncodeToString(d[:])
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err = tx.Rollback(); err != nil {
			logger.Error("Failed rollback")
		}
	}()
	_, err = tx.Exec("UPDATE voter SET voted=true WHERE om=$1", v.Om)
	if err != nil {
		logger.Error("Could not update voter")
		return err
	}
	_, err = tx.Exec("INSERT INTO vote(voterhash, candidate) VALUES ($1,$2)", hash, v.Candidate)
	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok {
			switch pqErr.Code.Name() {
			case "foreign_key_violation", "unique_violation":
				return ErrForeignKeyViolation
			}
		}
		logger.Error("Could not create vote record")
		return err
	}
	return tx.Commit()
}

func VoteStat() ([]*types.VoteStat, error) {
	stats := make([]*types.VoteStat, 0)
	err := db.Select(&stats, "SELECT candidate,ts FROM vote ORDER BY ts")
	if err != nil {
		logger.Error("Could not get vote stat data")
	}
	return stats, err
}
