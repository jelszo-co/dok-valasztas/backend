module gitlab.com/jelszo-co/dok-valasztas/backend

go 1.14

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/anargu/gin-brotli v0.0.0-20210111213823-a283cb19bf94
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/emvi/null v0.0.0-20210117151026-1bf8abf21c69
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/zap v0.0.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-openapi/spec v0.20.3 // indirect
	github.com/go-redis/redis/v8 v8.5.0
	github.com/gobuffalo/packr/v2 v2.8.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/karrick/godirwalk v1.16.1 // indirect
	github.com/lib/pq v1.2.0
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/rogpeppe/go-internal v1.7.0 // indirect
	github.com/rubenv/sql-migrate v0.0.0-20200616145509-8d140a17f351
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.3.0
	github.com/swaggo/swag v1.7.0
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a // indirect
	golang.org/x/sys v0.0.0-20210305034016-7844c3c200c3 // indirect
	golang.org/x/term v0.0.0-20201210144234-2321bbc49cbf // indirect
	golang.org/x/tools v0.1.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
)
