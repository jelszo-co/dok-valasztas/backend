/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package status

import (
	"gitlab.com/jelszo-co/dok-valasztas/backend/database"
	"gitlab.com/jelszo-co/dok-valasztas/backend/server/types"
	"time"
)

func StartTicker() {
	t := time.NewTicker(time.Minute * 30)
	TickerFunc()
	go func() {
		for {
			<-t.C
			TickerFunc()
		}
	}()
}
func TickerFunc() {

	curr, err := database.GetCurrentStatus()
	if err != nil {
		logger.Error("couldn't get current status")
		return
	}
	if curr != types.StatusAuto {
		logger.Info("status is not auto, skipping")
		return
	}
	timings, err := database.GetTimings()
	if err != nil {
		logger.Error("couldn't get timings")
		return
	}
	if len(timings) == 0 {
		logger.Warn("status is auto but no timing found, setting to default")
		err := database.SetCurrentStatus(types.StatusClosed)
		if err != nil {
			logger.Fatal("Could not set status to default, no valid status")
		}
		return
	}
	err = setStatusByTiming(timings)
	if err != nil {
		return
	}

}

func setStatusByTiming(timings []*types.StatusTiming) error {
	now := time.Now()
	logger.Info(now, timings[0].Start, timings[0].Until)
	for _, s := range timings {
		if s.Start.Before(now) && s.Until.After(now) {
			return setRedis(s)
		}
	}
	logger.Warn("timing not found for now because fuck Blemate")
	if timings[len(timings)-1].Until.Before(now) {
		i := getIndex(timings[len(timings)-1].Status)
		return setRedis(types.NewDummyTiming(i + 1))
	}
	if timings[0].Start.After(now) {
		i := getIndex(timings[0].Status)
		return setRedis(types.NewDummyTiming(i - 1))
	}
	logger.DPanicw("well... no idea", "timings", timings)
	return nil
}

func setRedis(s *types.StatusTiming) error {
	status = s.Status
	logger.Info("status set to ", s.Status)
	return nil
}

func getIndex(key types.StatusType) int {
	for i, t := range types.StatusArray {
		if t == key {
			return i
		}
	}
	logger.DPanic("not found in predefined status array")
	return 69420
}
