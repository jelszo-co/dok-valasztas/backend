/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package types

import (
	"github.com/emvi/null"
	"strings"
)

//Stores all candidate data
type Candidate struct {
	// The unique id of candidate, used for identifying.
	ID int `json:"id" example:"1"`
	// The name of the candidates.
	Name []string `json:"name" db:"-"`

	NameInternal string `json:"-" db:"name" swaggerignore:"true"`

	// The description provided by the candidate, intended as a short introduction.
	Description null.String `json:"description" swaggertype:"string" example:"Hello. Vote for me."`
	// The picture of the candidate.
	Picture null.String `json:"picture" swaggertype:"string" format:"base64"`
} // @name Candidate

type CandidateEssay struct {
	Essay string `json:"essay" format:"base64"`
}

//In transforms Candidate data from REST format to db format
func (c *Candidate) In() {
	c.NameInternal = strings.Join(c.Name, ";")
}

//Out transforms Candidate data from db format to REST format
func (c *Candidate) Out() {
	c.Name = strings.Split(c.NameInternal, ";")
}

type CandidateResult struct {
	// The unique id of candidate, used for identifying.
	ID     int `json:"id" example:"1" db:"id"`
	Result int `json:"result" example:"69"`
}
