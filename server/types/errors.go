/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package types

//Error structure to be returned in case of a non 2xx status code.
type Error struct {
	Error ErrorType `json:"error" enums:"NOT_FOUND,METHOD_NOT_ALLOWED,SERVER_ERROR,INVALID_AUTH_HEADER,EXPIRED_TOKEN,INVALID_BODY,ALREADY_VOTED,NO_SUCH_VOTER,TOO_MANY_TRIES,NO_SUCH_CANDIDATE,WRONG_STATUS" example:"IMAGINE_THE_CORRECT_ONE_HERE"`
} // @name Error

//Type for const errors.
type ErrorType string

//Error types which are present in the swagger enums.
const (
	ErrNotFound          ErrorType = "NOT_FOUND"
	ErrMethodNotAllowed  ErrorType = "METHOD_NOT_ALLOWED"
	ErrServerError       ErrorType = "SERVER_ERROR"
	ErrInvalidAuthHeader ErrorType = "INVALID_AUTH_HEADER"
	ErrExpiredToken      ErrorType = "EXPIRED_TOKEN"
	ErrInvalidBody       ErrorType = "INVALID_BODY"
	ErrBadPasscode       ErrorType = "BAD_PASSCODE"
	ErrAlreadyVoted      ErrorType = "ALREADY_VOTED"
	ErrNoSuchVoter       ErrorType = "NO_SUCH_VOTER"
	ErrTooManyTries      ErrorType = "TOO_MANY_TRIES"
	ErrNoSuchCandidate   ErrorType = "NO_SUCH_CANDIDATE"
	ErrWrongStatus       ErrorType = "WRONG_STATUS"
)
