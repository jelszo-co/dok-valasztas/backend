/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package types

import "os"

//JSON object for receiving passcode
type Passcode struct {
	Passcode string `json:"passcode" binding:"required"`
} // @name Passcode

//Validates, if the passcode is valid
func (p *Passcode) Validate() bool {
	pc := os.Getenv("PASSCODE")
	return pc != "" && pc == p.Passcode
}

//JSON object for returning an auth token
type Token struct {
	Token string `json:"token"`
} // @name Token
