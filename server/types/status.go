/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package types

import (
	"time"
)

//Type to be returned to /status/current
type StatusCurrent struct {
	Status StatusType `json:"current" enums:"CLOSED,OPEN,VOTE,VOTE_EXPIRED,RESULT" example:"CLOSED" binding:"required"`
} // @name CurrentStatus

//Type for status types
type StatusType string

//Status types in swagger enum
const (
	StatusClosed      StatusType = "CLOSED"
	StatusOpen        StatusType = "OPEN"
	StatusVote        StatusType = "VOTE"
	StatusVoteExpired StatusType = "VOTE_EXPIRED"
	StatusResult      StatusType = "RESULT"
	StatusAuto        StatusType = "AUTO"
)

var statusTypes = map[StatusType]struct{}{
	StatusClosed:      {},
	StatusOpen:        {},
	StatusVote:        {},
	StatusVoteExpired: {},
	StatusResult:      {},
}

var StatusArray = []StatusType{
	StatusClosed,
	StatusOpen,
	StatusVote,
	StatusVoteExpired,
	StatusResult,
}

//IsValidStatus returns whether stat is a declared status.
func IsValidStatus(stat StatusType) bool {
	_, ok := statusTypes[stat]
	return ok
}

type StatusTiming struct {
	Status StatusType `json:"status" db:"name" enums:"CLOSED,OPEN,VOTE,VOTE_EXPIRED,RESULT" example:"CLOSED" binding:"required"`
	Start  time.Time  `json:"start" db:"ts_start" binding:"required"`
	Until  time.Time  `json:"until" db:"ts_until" binding:"required"`
}

func NewDummyTiming(i int) *StatusTiming {
	if i == 0 && i == len(StatusArray)-1 {
		return &StatusTiming{Status: StatusClosed}
	}
	return &StatusTiming{Status: StatusArray[i]}
}
