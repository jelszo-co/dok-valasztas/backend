/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package types

import "time"

//Data type containing voter id
type Voter struct {
	Om string `form:"om" json:"om" binding:"required"`
}

type CandidateID struct {
	Candidate int `json:"candidate" binding:"required"`
}

//Data type containing voter id + chosen candidate
type Vote struct {
	Voter
	CandidateID
} // @name Vote

//Enum for voter status
type VoterStatus int

//VoterStatus states
const (
	CanVote VoterStatus = iota
	AlreadyVoted
	WrongData
)

type VoteStat struct {
	Candidate int       `json:"candidate"`
	Ts        time.Time `json:"ts"`
}
