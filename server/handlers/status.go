/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/jelszo-co/dok-valasztas/backend/database"
	"gitlab.com/jelszo-co/dok-valasztas/backend/server/types"
	"gitlab.com/jelszo-co/dok-valasztas/backend/status"
)

// GetCurrentStatus godoc
// @Summary Get current status
// @Tags Status
// @Produce json
// @Success 200 {object} types.StatusCurrent
// @Failure 500 {object} types.Error
// @Router /status/current [get]
func (h *handler) GetCurrentStatus(c *gin.Context) {
	curr, err := database.GetCurrentStatus()
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	if curr != types.StatusAuto {
		c.JSON(200, types.StatusCurrent{Status: curr})
		return
	}
	curr, err = status.GetStatus()
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	c.JSON(200, types.StatusCurrent{Status: curr})
}

// SetCurrentStatus godoc
// @Summary Set current status
// @Tags Status
// @Accept  json
// @Produce json
// @Security AdminToken
// @Param Status body types.StatusCurrent false "New status."
// @Success 200 {object} types.StatusCurrent
// @Failure 400,403,500 {object} types.Error
// @Router /status/current [post]
func (h *handler) SetCurrentStatus(c *gin.Context) {
	status := new(types.StatusCurrent)
	err := c.BindJSON(status)
	if err != nil || !types.IsValidStatus(status.Status) {
		ErrorHandler(c, 400, types.ErrInvalidBody)
		return
	}
	err = database.SetCurrentStatus(status.Status)
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	err = database.ClearTimings()
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	h.L.Info("Status set to ", status.Status)
	h.GetCurrentStatus(c)
}

// GetTimings godoc
// @Summary Get status timings
// @Tags Status
// @Produce json
// @Success 200 {array} types.StatusTiming
// @Failure 500 {object} types.Error
// @Router /status/timing [get]
func (h *handler) GetTimings(c *gin.Context) {
	timings, err := database.GetTimings()
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	c.JSON(200, timings)
}

// AddTiming godoc
// @Summary Add or update status timing
// @Tags Status
// @Accept json
// @Produce json
// @Security AdminToken
// @Param Timing body types.StatusTiming false "New timing."
// @Success 200 {array} types.StatusTiming
// @Failure 500 {object} types.Error
// @Router /status/timing [post]
func (h *handler) AddTiming(c *gin.Context) {
	timing := new(types.StatusTiming)
	err := c.BindJSON(timing)
	if err != nil || !types.IsValidStatus(timing.Status) {
		ErrorHandler(c, 400, types.ErrInvalidBody)
		return
	}
	err = database.AddTiming(timing)
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	err = database.SetCurrentStatus(types.StatusAuto)
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	status.TickerFunc()
	h.GetTimings(c)
}

// DeleteTiming godoc
// @Summary Remove status timing
// @Description Does NOT trow error when deleting non-existent timing. If all timing is deleted, status is set to CLOSED.
// @Tags Status
// @Accept json
// @Produce json
// @Security AdminToken
// @Param Timing body types.StatusCurrent false "Status"
// @Success 200 {array} types.StatusTiming
// @Failure 500 {object} types.Error
// @Router /status/timing [delete]
func (h *handler) DeleteTiming(c *gin.Context) {
	t := new(types.StatusCurrent)
	err := c.BindJSON(t)
	if err != nil || !types.IsValidStatus(t.Status) {
		ErrorHandler(c, 400, types.ErrInvalidBody)
		return
	}
	err = database.RemoveTiming(t.Status)
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	status.TickerFunc()
	h.GetTimings(c)
}

func (h *handler) OnlyOnStatus(s types.StatusType) func(*gin.Context) {
	return func(c *gin.Context) {
		curr, err := database.GetCurrentStatus()
		if err != nil {
			ErrorHandler(c, 500, types.ErrServerError)
			return
		}
		if curr == types.StatusAuto {
			curr, err = status.GetStatus()
			if err != nil {
				ErrorHandler(c, 500, types.ErrServerError)
				return
			}
		}
		if s == curr {
			c.Next()
			return
		}
		h.L.Warnw("Tried to access endpoint in wrong status", "path", c.FullPath())
		c.Abort()
		ErrorHandler(c, 403, types.ErrWrongStatus)
	}
}
