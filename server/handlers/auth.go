/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/jelszo-co/dok-valasztas/backend/server/types"
	"os"
	"time"
)

//AuthMiddleware only allows requests which have a valid token.
func (h *handler) AuthMiddleware(c *gin.Context) {
	tokenRaw := c.GetHeader("Authorization")
	_, err := jwt.Parse(tokenRaw, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		if os.Getenv("SECRET") == "" {
			return nil, errors.New("no secret")
		}
		return []byte(os.Getenv("SECRET")), nil
	})
	if err != nil {
		h.L.Warnw("Invalid auth token", "path", c.FullPath(), "ip", c.ClientIP())
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors == jwt.ValidationErrorExpired {
				ErrorHandler(c, 403, types.ErrExpiredToken)
				return
			}
		}
		ErrorHandler(c, 403, types.ErrInvalidAuthHeader)
		return
	}
	h.L.Infow("Successful authorization", "path", c.FullPath(), "ip", c.ClientIP())
	c.Next()
}

// Login godoc
// @Summary Get an auth token
// @Tags Auth
// @Accept  json
// @Produce  json
// @Param Passcode body types.Passcode false "The admin passcode"
// @Success 200 {object} types.Token
// @Failure 403,500 {object} types.Error
// @Router /auth/login [post]
func (h *handler) Login(c *gin.Context) {
	pc := new(types.Passcode)
	err := c.BindJSON(pc)
	if err != nil {
		ErrorHandler(c, 400, types.ErrInvalidBody)
		return
	}
	if !pc.Validate() {
		h.L.Warnw("Invalid passcode", "path", c.FullPath(), "ip", c.ClientIP())
		ErrorHandler(c, 403, types.ErrBadPasscode)
		return
	}
	token, err := getNewToken()
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	h.L.Infow("Successful login", "ip", c.ClientIP())
	c.JSON(200, types.Token{Token: token})
}

// Renew godoc
// @Summary Renew an auth token
// @Tags Auth
// @Produce json
// @Security AdminToken
// @Success 200 {object} types.Token
// @Failure 403,500 {object} types.Error
// @Router /auth/renew [post]
func (h *handler) Renew(c *gin.Context) {
	token, err := getNewToken()
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	h.L.Infow("Successful renew", "ip", c.ClientIP())
	c.JSON(200, types.Token{Token: token})
}

func getNewToken() (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		IssuedAt:  time.Now().Unix(),
		NotBefore: time.Now().Unix(),
		ExpiresAt: time.Now().Add(time.Hour * 3).Unix(),
	})
	return token.SignedString([]byte(os.Getenv("SECRET")))
}
