/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/jelszo-co/dok-valasztas/backend/database"
	"gitlab.com/jelszo-co/dok-valasztas/backend/server/types"
	"strconv"
)

//Fail2BanMiddleware is a middleware which checks whether the ip is temporarily banned for failed tries.
func (h *handler) Fail2BanMiddleware(c *gin.Context) {
	banned, err := database.Fail2BanCheck(c.ClientIP())
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	if banned != -1 {
		h.L.Warnw("exceeded rate limit", "ip", c.ClientIP(), "path", c.FullPath())
		c.Header("Retry-After", strconv.Itoa(banned))
		ErrorHandler(c, 429, types.ErrTooManyTries)
		return
	}
	c.Next()
}

// CheckVote godoc
// @Summary Check whether voting with these data is possible
// @Tags Vote
// @Param Voter query types.Voter false "Voter data"
// @Produce json
// @Success 200 {object} types.Empty
// @Failure 400,403,429,500 {object} types.Error
// @Router /vote [get]
func (h *handler) CheckVote(c *gin.Context) {
	v := new(types.Voter)
	err := c.ShouldBind(v)
	if err != nil {
		ErrorHandler(c, 400, types.ErrInvalidBody)
		return
	}
	r, err := database.CheckVoter(v)
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	switch r {
	case types.CanVote:
		c.JSON(200, types.Empty{})
		return
	case types.AlreadyVoted:
		c.JSON(403, types.Error{Error: types.ErrAlreadyVoted})
		return
	case types.WrongData:
		err = database.Fail2BanFail(c.ClientIP())
		if err != nil {
			ErrorHandler(c, 500, types.ErrServerError)
			return
		}
		c.JSON(403, types.Error{Error: types.ErrNoSuchVoter})
		return
	}
	c.JSON(500, types.ErrServerError)
}

// Vote godoc
// @Summary Cast a vote
// @Tags Vote
// @Param Voter body types.Vote false "Voter data"
// @Accept json
// @Produce json
// @Success 200 {object} types.Empty
// @Failure 400,403,429,500 {object} types.Error
// @Router /vote [post]
func (h *handler) Vote(c *gin.Context) {
	v := new(types.Vote)
	err := c.ShouldBind(v)
	if err != nil {
		ErrorHandler(c, 400, types.ErrInvalidBody)
		return
	}
	r, err := database.CheckVoter(&v.Voter)
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	switch r {
	case types.AlreadyVoted:
		c.JSON(403, types.Error{Error: types.ErrAlreadyVoted})
		return
	case types.WrongData:
		err = database.Fail2BanFail(c.ClientIP())
		if err != nil {
			ErrorHandler(c, 500, types.ErrServerError)
			return
		}
		c.JSON(403, types.Error{Error: types.ErrNoSuchVoter})
		return
	}
	err = database.Vote(v)
	if err != nil {
		if err == database.ErrForeignKeyViolation {
			ErrorHandler(c, 400, types.ErrNoSuchCandidate)
			return
		}
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	c.JSON(200, types.Empty{})
}

// VoteStats godoc
// @Summary Return vote statistics
// @Tags Vote
// @Produce json
// @Security AdminToken
// @Success 200 {array} types.VoteStat
// @Failure 400,403,500 {object} types.Error
// @Router /vote/stats [get]
func (h *handler) VoteStats(c *gin.Context) {
	stats, err := database.VoteStat()
	if err != nil {
		ErrorHandler(c, 500, types.ErrServerError)
		return
	}
	c.JSON(200, stats)
}
