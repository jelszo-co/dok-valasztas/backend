/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"gitlab.com/jelszo-co/dok-valasztas/backend/database"
	"gitlab.com/jelszo-co/dok-valasztas/backend/server/types"
	"strconv"
)

// GetCandidates godoc
// @Summary Get all candidates
// @Tags Candidate
// @Produce  json
// @Success 200 {array} types.Candidate
// @Failure 500 {object} types.Error
// @Router /candidate [get]
func (h *handler) GetCandidates(c *gin.Context) {
	candidates, err := database.GetCandidates()
	if err != nil {
		c.JSON(500, types.Error{Error: types.ErrServerError})
		return
	}
	for _, c := range candidates {
		c.Out()
	}
	c.JSON(200, candidates)
}

// GetCandidateEssay godoc
// @Summary Get candidate essay
// @Tags Candidate
// @Produce  json
// @Param id path int false "Candidate id"
// @Success 200 {object} types.CandidateEssay
// @Failure 400,500 {object} types.Error
// @Router /candidate/essay/{id} [get]
func (h *handler) GetCandidateEssay(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(400, types.Error{Error: types.ErrInvalidBody})
		return
	}
	essay, err := database.GetCandidateEssay(id)
	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(400, types.Error{Error: types.ErrNoSuchCandidate})
			return
		}
		c.JSON(500, types.Error{Error: types.ErrServerError})
		return
	}
	c.JSON(200, essay)
}

// Winner godoc
// @Summary Get winner candidate(s)
// @Tags Candidate
// @Produce  json
// @Success 200 {array} types.CandidateID
// @Failure 500 {object} types.Error
// @Router /candidate/winner [get]
func (h *handler) Winner(c *gin.Context) {
	candidates, err := database.GetWinner()
	if err != nil {
		c.JSON(500, types.Error{Error: types.ErrServerError})
		return
	}
	c.JSON(200, candidates)
}

// GetResults godoc
// @Summary Get vote count for each candidate
// @Tags Candidate
// @Produce  json
// @Success 200 {object} map[int]int
// @Failure 403,500 {object} types.Error
// @Router /candidate/results [get]
func (h *handler) GetResult(c *gin.Context) {
	results, err := database.GetResults()
	if err != nil {
		c.JSON(500, types.Error{Error: types.ErrServerError})
		return
	}
	c.JSON(200, results)
}
