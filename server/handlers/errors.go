/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/jelszo-co/dok-valasztas/backend/server/types"
)

//ErrorHandler returns an error with the given status code and error text, and aborts the middleware chain.
func ErrorHandler(c *gin.Context, status int, error types.ErrorType) {
	c.JSON(status, types.Error{Error: error})
	c.Abort()
}
