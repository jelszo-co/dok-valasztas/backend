/*
 * DÖK választás backend server
 * Copyright (C) 2021 Bence Tóth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package server

import (
	brotli "github.com/anargu/gin-brotli"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.com/jelszo-co/dok-valasztas/backend/docs"
	"gitlab.com/jelszo-co/dok-valasztas/backend/server/handlers"
	"gitlab.com/jelszo-co/dok-valasztas/backend/server/types"
	"gitlab.com/jelszo-co/dok-valasztas/backend/tools"
	"go.uber.org/zap"
	"time"
)

/*

 */

// @title DÖK választás
// @version 1.0
// @description The backend server for my school's election.

// @contact.name Bence Tóth
// @contact.email me@bnctth.dev

// @license.name GPLv3
// @license.url https://www.gnu.org/licenses/

// @host 127.0.0.1:8080
// @BasePath /
// @query.collection.format multi

// @securityDefinitions.apikey AdminToken
// @in header
// @name Authorization

// @tag.name Candidate
// @tag.description All operations involving candidates

// @tag.name Auth
// @tag.description All admin auth endpoint

// @tag.name Status
// @tag.description All status endpoints

// @tag.name Vote
// @tag.description All voting related endpoints

//Run starts the gin server
func Run(prod bool, logger *zap.SugaredLogger) {
	if prod {
		gin.SetMode(gin.ReleaseMode)
		docs.SwaggerInfo.Host = tools.GetEnvDef("HOST", "127.0.0.1:8080")
		docs.SwaggerInfo.Schemes = []string{"http", "https"}
	}
	r := gin.New()
	h := handlers.NewHandler(logger)

	//logging
	r.Use(ginzap.Ginzap(logger.Desugar(), time.RFC3339, true))
	r.Use(ginzap.RecoveryWithZap(logger.Desugar(), true))

	//optional br compression
	r.Use(brotli.Brotli(brotli.DefaultCompression))

	corsconf := cors.DefaultConfig()
	corsconf.AllowAllOrigins = true
	corsconf.AllowHeaders = append(corsconf.AllowHeaders, "Authorization")
	corsconf.ExposeHeaders = append(corsconf.ExposeHeaders, "Retry-After")
	//of CORS
	r.Use(cors.New(corsconf))

	candidate := r.Group("/candidate")
	candidate.GET("", h.GetCandidates)
	candidate.GET("/essay/:id", h.GetCandidateEssay)
	candidate.GET("/winner", h.OnlyOnStatus(types.StatusResult), h.Winner)
	candidate.GET("/results", h.OnlyOnStatus(types.StatusResult), h.GetResult)

	auth := r.Group("/auth")
	auth.POST("/login", h.Login)
	auth.POST("/renew", h.AuthMiddleware, h.Renew)

	status := r.Group("/status")
	status.GET("/current", h.GetCurrentStatus)
	status.POST("/current", h.AuthMiddleware, h.SetCurrentStatus)
	status.GET("/timing", h.GetTimings)
	status.POST("/timing", h.AuthMiddleware, h.AddTiming)
	status.DELETE("/timing", h.AuthMiddleware, h.DeleteTiming)

	vote := r.Group("/vote", h.Fail2BanMiddleware)
	vote.GET("", h.OnlyOnStatus(types.StatusVote), h.CheckVote)
	vote.POST("", h.OnlyOnStatus(types.StatusVote), h.Vote)
	vote.GET("/stats", h.AuthMiddleware, h.VoteStats)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	r.NoRoute(func(c *gin.Context) {
		handlers.ErrorHandler(c, 404, types.ErrNotFound)
	})
	r.NoMethod(func(c *gin.Context) {
		handlers.ErrorHandler(c, 405, types.ErrMethodNotAllowed)
	})
	r.HandleMethodNotAllowed = true

	logger.Fatal(r.Run())
}
