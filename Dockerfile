FROM golang:1.15 AS builder

WORKDIR /go/src/gitlab.com/jelszo-co/dok-valasztas/backend
COPY . .
RUN go get -u github.com/gobuffalo/packr/v2/packr2 github.com/swaggo/swag/cmd/swag
RUN swag i -d server -g setup.go
RUN go get
RUN packr2 build -o application main.go

FROM ubuntu:20.04

RUN useradd -r app
WORKDIR /home/app
COPY --from=builder /go/src/gitlab.com/jelszo-co/dok-valasztas/backend/application .
RUN chown -R app:app /home/app

USER app

CMD ["/home/app/application"]
